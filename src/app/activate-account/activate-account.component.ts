import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserServiceService } from '../services/user-service.service';

@Component({
  selector: 'app-activate-account',
  templateUrl: './activate-account.component.html',
  styleUrls: ['./activate-account.component.css']
})
export class ActivateAccountComponent implements OnInit {
user:any
email:any
  constructor(private serviceUser:UserServiceService,private router:Router) { }

  ngOnInit(): void {
    this.email=localStorage.getItem('email')
  }
  activateAccount(){
 this.serviceUser.update_user(this.email).subscribe(data=>{
  this.user  = data
  console.log(data );

})

  }
}
