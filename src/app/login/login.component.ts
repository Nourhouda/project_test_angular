import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../services/user-service.service';
import { FormControl, FormGroup, FormBuilder,Validators} from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  registeruserForm:FormGroup
  active
  email:any
  user:any
  constructor(private formBuilder: FormBuilder,private serviceUser:UserServiceService,private router:Router) {
    this.createForm()
   }

  createForm() {
    this.registeruserForm = this.formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email:['', Validators.required],
      password:['', Validators.required]
    });
}
  ngOnInit(): void {
    this.email=localStorage.getItem('email')
  }
  login() {
    console.log('value first name')
    console.log(this.registeruserForm.value)
    this.serviceUser.find_user(this.email).subscribe(data=>{
      this.user  = data
      console.log(data );
      localStorage.setItem('firstname',data.firstname)
      localStorage.setItem('lastname',data.lastname)
      localStorage.setItem('email',data.email)
      this.active=data.active
      if(this.active=='1'){
        this.router.navigateByUrl('profil')
      }else{
        this.router.navigateByUrl('login')
      }
    })




  }
}
