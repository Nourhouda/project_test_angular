import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {
firstName
lastname
email
  constructor() { }

  ngOnInit(): void {
   this.firstName= localStorage.getItem('firstname')
    this.lastname=  localStorage.getItem('lastname')
    this.email=  localStorage.getItem('email')
  }

}
