import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder,Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { UserServiceService } from '../services/user-service.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

registeruserForm:FormGroup
  constructor(private formBuilder: FormBuilder,private serviceUser:UserServiceService,private router:Router) {
    this.createForm()
   }
   createForm() {
    this.registeruserForm = this.formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email:['', Validators.required],
      password:['', Validators.required]
    });
}
//get f() { return this.registeruserForm.controls; }
  ngOnInit() {

  }
  onSubmit() {
    console.log('value first name')
    console.log(this.registeruserForm.value)
    this.serviceUser.create_user().subscribe(data=>{
      localStorage.setItem('firstname',this.registeruserForm.value.firstname)
      localStorage.setItem('lastname',this.registeruserForm.value.lastname)
      localStorage.setItem('email',this.registeruserForm.value.email)
      localStorage.setItem('active','0')
      this.serviceUser.send_mail(this.registeruserForm.value.email)
     this.router.navigateByUrl('activate')

    })


  }

}
