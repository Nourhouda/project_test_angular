import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  constructor(private httpClient: HttpClient,private router:Router) {

  }
  create_user(user){
    const headers = new HttpHeaders;
    const body={"firstname":user.firstname,
    "lastname":user.lastname,
    "email":user.email,
    "password":user.password,
    "active":0}
   this.httpClient.post('http://localhost:8999/add-user',body,{ headers: headers }) .pipe(res => res)
  }
  find_user(email){
    const headers = new HttpHeaders;
    const body={"email":email}
    this.httpClient.post('http://localhost:8999/api/user-email',body,{ headers: headers }) .pipe(res => res)
  }
  update_user(user){
    const headers = new HttpHeaders;
    const body={"email":user.userKey,"active":1}
    this.httpClient.post('http://localhost:8999/api/user-update',body,{ headers: headers }) .pipe(res => res)
  }
  send_mail(email){
    const headers = new HttpHeaders;
    const body= {"to":email ,
    "from": "etudiant.nourhouda.sallami@uvt.tn",
    "subject": "Activate Account ",
     "html":"<h1>please click here to activate your Account</h1><a href='http://localhost:4200/activate-account'>click here</a>"}
    this.httpClient.post('http://localhost:8999/api/send-mail/',body,{ headers: headers }).pipe(res => res)

  }
}

